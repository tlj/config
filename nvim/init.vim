syntax on
set ruler
set formatoptions+=o 	" continue comment marker in new lines
set modeline            " Enable modeline.
set linespace=0         " Set line-spacing to minimum.
set noerrorbells                " No beeps
set encoding=utf-8              " Set default encoding to UTF-8
set showmode                    " Show current mode.
set nostartofline       " Do not jump to first character with page commands.
set noswapfile                  " Don't use swapfile
set nobackup            	" Don't create annoying backup files
set fileformats=unix,dos,mac    " Prefer Unix over Windows over OS 9 formats
set hlsearch                    " Highlight found searches
set incsearch                   " Shows the match while typing
set ignorecase                  " Search case insensitive...
set smartcase                   " ... but not when search pattern contains upper case characters
set autoindent
set tabstop=4 shiftwidth=4 expandtab
set nospell
set mouse=

" plguins here
call plug#begin('~/.config/nvim/plugged')
Plug 'sheerun/vim-polyglot'
Plug 'mhartington/oceanic-next'
Plug 'lifepillar/vim-solarized8'
Plug 'joshdick/onedark.vim'
Plug 'vim-airline/vim-airline'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fugitive'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-go', { 'do': 'make'}
"Plug 'srstevenson/vim-picker'
Plug 'junegunn/fzf'
"Plug 'terryma/vim-multiple-cursors'
Plug 'editorconfig/editorconfig-vim'
Plug 'mattn/emmet-vim'
Plug 'airblade/vim-gitgutter'
Plug 'padawan-php/deoplete-padawan', { 'do': 'composer install' }
call plug#end()

let g:deoplete#enable_at_startup = 1

let mapleader=","

" Toggle NERD Tree
noremap <leader>t :NERDTreeToggle<CR>
" Fuzzy find files
noremap <leader>p :FZF<CR>
" toggle line number
nnoremap <silent> <leader>n :set number! number?<CR>
" run the currently edited shell script
nnoremap <leader>r :!%:p<CR>

" Or if you have Neovim >= 0.1.5
"if (has("termguicolors"))
" set termguicolors
"endif

" Theme
syntax enable
set background=dark
colorscheme solarized8

" Airline
let g:airline#extensions#tabline#enabled = 2
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#right_sep = ' '
let g:airline#extensions#tabline#right_alt_sep = '|'
let g:airline_powerline_fonts=1
let g:airline_left_sep = ' '
let g:airline_left_alt_sep = '|'
let g:airline_right_sep = ' '
let g:airline_right_alt_sep = '|'
let g:airline_powerline_fonts=1
let g:airline_theme='oceanicnext'

