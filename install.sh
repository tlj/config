#!/bin/bash

install_neovim() {
    echo "Installing NeoVIM..."
    brew install neovim
    brew install python
    /usr/local/bin/pip3 install --upgrade neovim
}

setup_neovim() {
    echo "Setting up neovim..."
    mkdir -p ~/.config/nvim
    ln -s $(pwd)/nvim/init.vim ~/.config/nvim/init.vim 2> /dev/null
    curl -sfLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    nvim +'PlugInstall --sync' +qa
    grep -q -F 'alias vim="nvim"' ~/.zshrc || echo 'alias vim="nvim"' >> ~/.zshrc
}

install_neovim
setup_neovim

if [ ! -z "$ZSH_CUSTOM" ]; then
    git clone git@github.com:superbrothers/zsh-kubectl-prompt.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-kubectl-prompt
    ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"
fi

ln -s `pwd`/tmux.conf ~/.tmux.conf
ln -s `pwd`/zshrc ~/.zshrc
